package com.safebear.auto.pages;

import com.safebear.auto.pages.locators.ToolsPageLocators;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

@RequiredArgsConstructor
public class ToolsPage {

    @NonNull
    WebDriver driver;

    ToolsPageLocators toolsPageLocators = new ToolsPageLocators();

    //Navigate to page
    public String getPageTitle(){
        return driver.getTitle().toString();
    }

    //Validating the successful login and the landing page displayed
    public String checkForLoginSuccessfulMessage(){
        return driver.findElement(toolsPageLocators.getLoginsuccessfulmessage()).getText();
    }

    // Adding text to the search field
     public void  enterSearchText(String toolName){
     driver.findElement(toolsPageLocators.getSearchTextFeild()).sendKeys(toolName);
       }

     //Selecting the search button
     public void selectSearchButton(){
        driver.findElement(toolsPageLocators.getSelectSearchButton()).click();
       }

     //Ensuring the search result is returned

     public boolean checkforSeleniumTool(){
     return driver.findElement(toolsPageLocators.getSearchResultDisplayed()).isDisplayed();
     }

//     //Click the additional exercises button
//     public void selectAdditionalExercisesButton(){
//        driver.findElement(toolsPageLocators.getSelectAdditionalExercisesButton()).click();
//
//     }
//
//        public String checkForAdditionalExercisesMessage(){
//        //Need to either create a new page or add additional exercises message to wrong page
//        return driver.findElement(toolsPageLocators
//
//        }


}


