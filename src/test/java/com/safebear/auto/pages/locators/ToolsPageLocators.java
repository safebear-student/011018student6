package com.safebear.auto.pages.locators;

import lombok.Data;
import org.openqa.selenium.By;

@Data
public class ToolsPageLocators {


    //Step to identify login successful message
    private By loginsuccessfulmessage = By.xpath ("//*['class=container']/p/b");

    //Identify search box
    private By searchTextFeild = By.id("toolName");

    //identify search button
    private By selectSearchButton = By.xpath(".//button[@class='btn btn-info']");

    //identify search result
    private By searchResultDisplayed = By.xpath(".//td[.='Selenium']");

//    //identify additional exercises button
//    private By selectAdditionalExercisesButton = By.xpath(".//button[@class='btn btn-lg btn-success']");

    //Select the "Say Something" button
}






