package com.safebear.auto.pages.locators;

import lombok.Data;
import org.openqa.selenium.By;

@Data
public class LoginPageLocators {

    // Adding steps to identify buttons on a page by ID's
    private By usernamelocatorfield  = By.id("username");
    private By passwordlocatorfield  = By.id("password");
    private By submitbuttonlocator   = By.id("enter");
    private By loginerrormessage     = By.xpath("//*[@id='rejectLogin']/b");

}
