package com.safebear.auto.pages;

import com.safebear.auto.pages.locators.LoginPageLocators;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

// Created object
@RequiredArgsConstructor
public class LoginPage {
        LoginPageLocators loginPageLocators = new LoginPageLocators();

    @NonNull
    WebDriver driver;

    //telling the driver to go to the page
    public String getPageTitle(){
        return driver.getTitle().toString();
    }

    //Getting username form page locator field
    public void enterUserName(String username){
        driver.findElement(loginPageLocators.getUsernamelocatorfield()).sendKeys(username);
    }

    //Getting password from page locator field
    public void enterPassword(String password){
        driver.findElement(loginPageLocators.getPasswordlocatorfield()).sendKeys(password);
    }

    //Selecting the login page
    public void selectSubmit(){
        driver.findElement(loginPageLocators.getSubmitbuttonlocator()).click();
    }

    //Validating error message for incorrect password
    public String checkforFailedLoginWarning(){
        return driver.findElement(loginPageLocators.getLoginerrormessage()).getText();
    }

}
