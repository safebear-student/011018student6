package com.safebear.auto.tests;

import com.cucumber.listener.Reporter;
import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.testng.annotations.AfterClass;

import java.io.File;

//Cucumber settings
@CucumberOptions(
        // Setting the location and style of the report
        // plugin = {"pretty", "html:target/cucumber"},   <--  Original report type
        plugin = {"com.cucumber.listener.ExtentCucumberFormatter:target/cucumber/extent_report.html"},
        //Setting the tags to be used in the tests
        tags= "~@to-do",
        // links feature file to test code
        glue = "com.safebear.auto.tests",
        //Feature file location
        features = "classpath:toolslist.features"
)

public class RunCukes extends AbstractTestNGCucumberTests {

        // Generating report of test results
        @AfterClass
        public static void tearDown() {
            Reporter.loadXMLConfig(new File("src/test/resources/extentReports/extent-config.xml"));
            Reporter.setSystemInfo("user", System.getProperty("user.name"));
            Reporter.setSystemInfo("os", "Windows");
            Reporter.setTestRunnerOutput("Sample test runner output message");
        }


}
