package com.safebear.auto.tests;

import com.safebear.auto.Tool;
import com.safebear.auto.pages.LoginPage;
import com.safebear.auto.pages.ToolsPage;
import com.safebear.auto.utils.Utils;
import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class StepDefs {

    //linking objects to StepDefs

    WebDriver driver;
    LoginPage loginPage;
    ToolsPage toolsPage;

    //Opening browser before running the tests
    @Before
    public void setUp() {
        driver = Utils.getDriver();
        driver.get(Utils.getUrl());

        loginPage = new LoginPage(driver);
        toolsPage = new ToolsPage(driver);
    }

    // Closing the browser after the tests have stopped running
    // Also including the wait time before closing the browser

    @After
    public void tearDown() {
        try {
            Thread.sleep(Integer.parseInt(System.getProperty("sleep", "2000")));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.quit();
    }

    //Multiple login tests for valid and failure

    @Given("^I navigate to the login page$")
    public void i_navigate_to_the_login_page() throws Throwable {
        //Action: Open our web application in a browser
        Assert.assertEquals("Login Page", loginPage.getPageTitle(), "We're not on the title page or the title has changed");
    }

    @When("^I enter the login details for a '(.+)'$")
    public void i_enter_the_login_details_for_a_USER(String user) throws Throwable {
        switch (user) {
            case "invalidUser":
                loginPage.enterUserName("attack");
                loginPage.enterPassword("WRONG");
                loginPage.selectSubmit();
                break;
            case "validUser":
                loginPage.enterUserName("tester");
                loginPage.enterPassword("letmein");
                loginPage.selectSubmit();
                break;
            default:
                Assert.fail("The test data is wrong - only values that can be accepted are 'validUser' or 'inalidUser'");
                break;
        }
    }

    @Then("^I can see the following message '(.+)'$")
    public void i_can_see_the_following_MESSAGE(String message) {
        switch (message) {

            case "Username or Password is incorrect":
                Assert.assertTrue(loginPage.checkforFailedLoginWarning().contains(message));
                break;
            case "Login Successful":
                Assert.assertTrue(toolsPage.checkForLoginSuccessfulMessage().contains(message));
                break;
            default:
                Assert.fail("The test data is wrong");
                break;

        }

    }



    // Search Feature --   Enter text into search box

    @Given("^I navigate to the search page$")
    public void i_navigate_to_the_search_page() throws Throwable {
        loginPage.enterUserName("tester");
        loginPage.enterPassword("letmein");
        loginPage.selectSubmit();
        Assert.assertTrue(toolsPage.checkForLoginSuccessfulMessage().contains("Login Successful"));
    }

    @When("^I Search for the (.+) tool$")
    public void i_enter_text_into_the_search_field(String toolName) throws Throwable {
        toolsPage.enterSearchText(toolName);
        toolsPage.selectSearchButton();
    }

    @Then("^The (.+) tool is returned$")
    public void correct_results_must_be_displayed(String toolName) throws Throwable {
       Assert.assertTrue(toolsPage.checkforSeleniumTool());

    }


//////  Adding javascript text to field test
////
////    @Given("^I am viewing additional exercises screen$")
////    public void i_am_viewing_additional_exercises_screen() throws Throwable {
////        loginPage.enterUserName("tester");
////        loginPage.enterPassword("letmein");
////        loginPage.selectSubmit();
////        toolsPage.selectAdditionalExercisesButton();
////
////        //Assert.assertTrue(toolsPage.checkForLoginSuccessfulMessage().contains("Login Successful"));
//
//
//
//    }
//
//    @When("^I add text to the field$")
//    public void i_add_text_to_the_field() throws Throwable {
//        // Write code here that turns the phrase above into concrete actions
//        throw new PendingException();
//    }
//
//    @Then("^it is displayed on screen$")
//    public void it_is_displayed_on_screen() throws Throwable {
//        // Write code here that turns the phrase above into concrete actions
//        throw new PendingException();
//    }

}

