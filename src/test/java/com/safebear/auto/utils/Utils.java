package com.safebear.auto.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Utils {

        // Setting base URL to be used as default in the tests
    private static final String URL = System.getProperty("url",
                "http://toolslist.safebear.co.uk:8080");


    // Setting the default browser as Chrome
    private static final String BROWSER = System.getProperty("browser", "chrome");


        // Opening the URL to be used in the test
    public static String getUrl() {
        return URL;
    }


    public static WebDriver getDriver() {
        //This is the location of my Chromedriver
        System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe");
        //This is the locaton of my Firefox driver
        System.setProperty("webdriver.gecko.driver", "src/test/resources/drivers/geckodriver.exe");



        //These are my Chrome options for running my tests
        ChromeOptions options = new ChromeOptions();
        options.addArguments("window-size=1366,768");



        switch (BROWSER) {
            case "chrome":
                return new ChromeDriver(options);
            // To run the tests via Maven without launching Maven
             case "Headless":
             options.addArguments("headless", "disable-gpu");
                return new ChromeDriver(options);

             // Adding a new driver to use Firefox
             case "Firefox":
                 return new FirefoxDriver();

            //Setting default driver
            default:
                return new ChromeDriver(options);
        }

    }

}

