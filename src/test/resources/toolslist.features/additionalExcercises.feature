#Feature: Javascript Input
#
#  Rules:
#  * When you add text to the say something field it is displayed on screen
#  * User must be logged into the system
#  * User must be viewing the additional exercises field
#
#  @to-do
#  @HighRisk
#  @HighImpact
#  Scenario: A user can add text to the Java input field
#    Given I am viewing additional exercises screen
#    When I add text to the field
#    Then it is displayed on screen