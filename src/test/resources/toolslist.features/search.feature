Feature: Searching for text
  Rules:
  *When you search for a tool that exists then it is the only one displayed in the list
  *You must be logged into search for a tool
  * The selenium tool exists


  @New
  @HighRisk
  @HighImpact
  Scenario: A user searches for a tool
    Given I navigate to the search page
    When I Search for the Selenium tool
    Then The selenium tool is returned